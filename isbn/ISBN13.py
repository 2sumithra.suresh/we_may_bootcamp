HYPHEN = '-'

def clean(isbn: str) -> list[int]:
    return [int(ch) for ch in isbn if ch != HYPHEN]

def convert_num_sum(num_list: list[int]) -> list[int]:
    for i in range(len(num_list)):
        if i % 2 == 1:
            num_list[i] *= 3
    return sum(num_list)

def check_digit(isbn: str) -> int:
    return (10 - (convert_num_sum(clean(isbn)) % 10)) % 10

print(check_digit("978-0-306-40615"))

