def digits_sum(n: int) -> int:
    # d = n % 9
    # return 9 if d == 0 else d
    return 0 if n == 0 else 1 + (n - 1) % 9

def sum_of_digits(num: int) -> int:
    return sum([int(i) for i in str(num)])
    
print(digits_sum(0))

print(-1 % 9)

def convert_num(num: int) -> int:
    return int(''.join(reversed([i if str(num).index(i) == 1 else str(digits_sum(2 * int(i))) for i in reversed(str(num))])))

def check_digit(num: int) -> int:
    return 10 - ((digits_sum(convert_num(num))) % 10)

print(convert_num(1789372997))
print(check_digit(1789372997))
