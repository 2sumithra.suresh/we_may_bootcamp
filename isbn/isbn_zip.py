HYPHEN = "-"

def clean(isbn: str) -> list[int]:
    return [int(ch) for ch in isbn if ch != HYPHEN]

def check_isbn(isbn: str) -> bool:
    return check_digit(isbn[:-1]) == int(isbn[-1])

def check_digit(isbn: str) -> int:
    weights = [1,3] * 6
    check_digit = sum([a * b for (a, b) in zip(weights, clean(isbn))]) % 10
    return (10 - check_digit) % 10

print(check_digit("978-0-306-40615"))
