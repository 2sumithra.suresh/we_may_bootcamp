import csv
import random

ALPHABETS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

def get_data(filename: str):
    with open(filename, 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        fields = next(csvreader)
        limit = 0
        perc_prob = {}
        weights = []
        for row in csvreader:
            perc = row[1][:-1:]
            perc_prob[row[0]] = float(perc)
            row = (row[0], (limit, float(perc) + limit))
            weights.append(row)
            limit += float(perc)
    return weights, perc_prob

def weighted_generate(weights: list[tuple]) -> str:
    prob = random.random() * 100
    for i in weights:
        lower, upper = i[1]
        if lower <= prob < upper:
            return i[0]

def random_str(strlen: int, weights: list[tuple]) -> str:
    string = ''
    for i in range (strlen):
        string += weighted_generate(weights)
    return string

def calc_perc(string:str) -> float:
    new_perc = dict.fromkeys(list(ALPHABETS), 0)
    for i in string:
        new_perc[i] = (string.count(i))/len(string) * 100
    return new_perc

def compare(new_perc: dict, perc_prob: dict) -> list[tuple]:
    compared_prob = []
    for i in ALPHABETS:
        compared_prob.append((i, perc_prob[i], new_perc[i]))
    return compared_prob

def get_string(filename: str) -> str:
    with open (filename, 'r') as textfile:
        string = textfile.read()
    return ''.join([char for char in string if char.isalpha()])

def format_print(result: list[tuple]):
    for i in result:
        print(i)
    print()
    
weights, perc_prob = get_data('letter_frequencies.csv')
string = random_str(50, weights)

format_print(compare(calc_perc(get_string("mono_ciph016.txt")), perc_prob))
format_print(compare(calc_perc(get_string("mono_ciph036.txt")), perc_prob))
format_print(compare(calc_perc(get_string("mono_ciph056.txt")), perc_prob))
