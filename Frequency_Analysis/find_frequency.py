from collections import defaultdict as ddict

def get_string(filename: str) -> str:
    with open (filename, 'r') as textfile:
        string = textfile.read()
    return ''.join([char for char in string if char.isalpha()])

def calc_perc(string:str) -> float:
    freq = ddict()
    for i in string:
        if i in freq.keys():
            freq[i] += 1
        else:
            freq[i] = 1
    return freq

file1 = "mono_ciph016.txt"
file2 = "mono_ciph036.txt"
file3 = "mono_ciph056.txt"

str1 = get_string(file1)
str2 = get_string(file2)
str3 = get_string(file3)

dict1 = calc_perc(str1)
dict2 = calc_perc(str2)
dict3 = calc_perc(str3)

for i in dict1.keys():
    print(i)
    text = f"{i}\t{dict1[i]}\t{dict2[i]}\t{dict3[i]}"
    print(text)
text = f'{dict1} {dict2} {dict3}'
print(text)
