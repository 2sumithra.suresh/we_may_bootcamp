import random

def generate_string(limit: int) -> str:
    string = ''
    for i in range (limit):
        string += chr(random.randint(0, 127))
    return string

print(ord('z'))
random.seed(42)
print(generate_string(5))
