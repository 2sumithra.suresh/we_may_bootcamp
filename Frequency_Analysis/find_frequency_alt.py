from collections import defaultdict as ddict
import sys

freq = ddict(list)
for i, arg in enumerate(sys.argv[1:]):
    for ch in open(arg).read():
        if ch not in freq:
            freq[ch] = [0, 0, 0]
        else:
            freq[ch][i] += 1

for ch in freq:
    print(f'{ch:3} {freq[ch][0]:6} {freq[ch][1]:6} {freq[ch][2]:6}')
