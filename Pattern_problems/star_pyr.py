import sys
LINEWIDTH = 40
STAR = "*"
SPACE = " "
LF = "\n"
# START, REPEAT, LAST = "#", "_", "#"
START, REPEAT, LAST = STAR, STAR + STAR, ""
# START, REPEAT, LAST = STAR, SPACE + STAR,""

def pattern(size: int):
    return [line(i) for i in range(size + 1)]

def line(line_num: int) -> str:
    if line_num == 0:
        return START
    return start(line_num - 1) + repeat(line_num - 1) + last(line_num - 1)

def start(n: int) -> str:
    return START

def repeat(n: int) -> str:
    return REPEAT * (2 * n + 1)

def last(n: int) -> str:
    return LAST

def make_pyramid(size: int):
    return LF.join([line.center(LINEWIDTH) for line in pattern(size)])

def make_right(size: int) -> str:
    return LF.join([line.rjust(LINEWIDTH) for line in pattern(size)])

def make_arrow(size: int) -> str:
    base = pattern(size)
    arrow = base + base[::-1][1:]
    return LF.join([line.center(LINEWIDTH) for line in arrow])
    
for arg in sys.argv[1:]:
    print(make_pyramid(int(arg)))   

# print(make_pyramid(7))
# print(make_right(7))
# print(make_arrow(7))
