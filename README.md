1. Asc_Desc - Identify increasing and decreasing strings and classify them as Ascending, Descending, Peak, Valley and None(X)

2. Convert_Equations - Arranges equation in increasing or decreasing order of the power of its terms

3. Crossword - Numbers an empty crossword

4. Decryption_Problems - Decrypts a substition cypher on a text file using frequency analysis

5. Frequency_Analysis - Finds the frequency of alphabets in a text file and generates a random string using weighted probabilities given

6. ISBN - Computes check_digit for ISBN-10 and ISBN-13

7. Number_Theory_Problems - Problems involving number theory like implementing Erastosthenes' Sieve,
    finding modular inverse, euclid's gcd, jailer;s problem, Fair Ratios problem and generating primes

8. Pattern_Problems - Creating different triangular patterns with a common base code

9. Student_Ranking_Problems - Ranking students based on their grades
