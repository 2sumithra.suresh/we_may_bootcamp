"""
def generate_primes(limit: int) -> list[int]:
    nums = [num for num in range(2, limit)]
    for factor in nums:
        while factor * factor <= limit:
            for i in range(factor ** 2, limit):
                if i in nums and i % factor == 0:
                    nums.remove(i)
    return nums
"""

def sieve(limit):
    numbers = list(range(2, limit))

    for num in numbers:
        if num != None:
            n = num
            while num * n < limit:
                numbers[num * n - 2] = None
                n += 1

    return [num for num in numbers if num]

print(sieve(1000000))
# print(generate_primes(10000))
