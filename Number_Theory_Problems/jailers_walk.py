OPEN, CLOSE = 1, 0

cells = [int(i) for i in ('0' * 100)]

def change_state(cells: list, step: int) -> list:
    def switch_state(state: int) -> int:
        return 1 if state == 0 else 0
    change_indexes = [i - 1 for i in range(1, len(cells)) if i % step == 0]
    for i in change_indexes:
        cells[i] = switch_state(cells[i])
    return cells

print(change_state(cells, 2))
