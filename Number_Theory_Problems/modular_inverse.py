def modular_inverse(m: int, n: int) -> int:
    r1, r2 = m, n
    y1, y2 = 0, 1
    while r2 != 1:
        q = r1 // r2
        r1, r2 = r2, r1 % r2
        y1, y2 = y2, y1 - q * y2

    return y2 if y2 > 0 else m + y2

print(modular_inverse(15, 79))
print(modular_inverse(79, 15))
