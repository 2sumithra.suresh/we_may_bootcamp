def is_ascending(strings: str) -> bool:
    return all(a<b for a,b in zip(strings, strings[1:]))

def is_descending(strings: str) -> bool:
    return all(a>b for a,b in zip(strings, strings[1:]))
    
def no_of_peaks(strings: str) -> bool:
    return sum(a<b>c for a,b,c in zip(strings,strings[1:], strings[2:]))

def no_of_valleys(strings: str) -> bool:
    return sum(a>b<c for a,b,c in zip(strings, strings[1:], strings[2:]))

def is_valley(strings: str) -> bool:
    first, second = strings.split(max(s), maxsplit = 1)
    return is_descending(first) and is_descending(second)
