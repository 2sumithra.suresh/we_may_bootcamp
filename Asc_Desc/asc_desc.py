def asc(word: str) -> bool:
    char = word[0]
    for i in word:
        if char > i:
            return False
        char = i
    return True


def desc(word: str) -> bool:
    char = word[0]
    for i in word:
        if char < i:
            return False
        char = i
    return True


def peak(word: str) -> bool:
    char = word[0]
    peak = max(word)
    for i in range(1, word.index(peak) + 1):
        if char > word[i]:
            return False
        char = word[i]
    for i in range(word.index(peak) + 1, len(word)):
        if char < word[i]:
            return False
        char = word[i]
    return True


def valley(word: str) -> bool:
    char = word[0]
    peak = min(word)
    for i in range(1, word.index(peak) + 1):
        if char < word[i]:
            return False
        char = word[i]
    for i in range(word.index(peak) + 1, len(word)):
        if char > word[i]:
            return False
        char = word[i]
    return True


print(asc("123"))
print(desc("1234321"))
print(peak("1234321"))
print(valley("1234321"))
