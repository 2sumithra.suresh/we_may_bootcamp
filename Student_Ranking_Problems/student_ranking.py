def sum_sort(l1: list) -> list:
    return sorted(l1, key = lambda x: x[1] + x[2] + x[3])

def compare(s1: list, s2: list) -> str:
    i = 2
    flag = s1[1] < s2[2]
    while i < len(s1):
        if s1[i] < s2[i] != flag:
            return '#'
        i += 1
    if flag == True:
        return '<'
    else:
        return '>'

def output_list(student_list: list) -> str:
    str1 = student_list[0][0]
    for s1, s2 in zip(student_list, student_list[1:]):
        if compare(s1,s2) != '#':
            str1 += compare(s1, s2) + s2[0]
    return str1

def change(l1):
    lnew = []
    for x in l1:
        if x.isnumeric():
            lnew.append(int(x))
        else:
            lnew.append(x)
         
    
SPACE = ' '
student_list = []
for line in open("student_ranking_data.txt"):
    student = line.strip().split(SPACE)
    student_list.append(student)
    student_list = sorted(list(map(lambda x: change(x),student_list)))
print(sorted(student_list))
print(output_list(sorted(student_list)))

