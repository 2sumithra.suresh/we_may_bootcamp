def get_day_num(filename: str) -> int:
    return int(filename[3:])

def file_sort(file_list: list[str]) -> list[str]:
    return sorted(file_list, key = get_day_num)

print(file_sort(['day11', 'day1', 'day10','day9','day2','day22']))
