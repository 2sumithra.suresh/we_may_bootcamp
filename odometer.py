UPPER_VALUE = 789
LOWER_VALUE = 123

def check_valid(num: int) -> bool:
    return all(int(a) < int(b) for a, b in zip(str(num), str(num)[1:]))

def get_next(num: int) -> int:
    while num < UPPER_VALUE:
        num += 1
        if check_valid(num):
            break
    return num

def get_prev(num: int) -> int:
    while num > LOWER_VALUE:
        num -= 1
        if check_valid(num):
            break
    return num

def get_next_step(num: int, step: int) -> int:
    for i in range(step):
        num = get_next(num)
    return num

def get_prev_step(num: int, step: int) -> int:
    for i in range(step):
        num = get_prev(num)
    return num

def get_step(num1: int, num2: int):
    if check_valid(num1) and check_valid(num2) and num1 != num2:
        if num2 < num1:
            num1, num2 = num2, num1
        step = 1
        while num1 < UPPER_VALUE:
            if get_next(num1) == num2:
                break
            num1 = get_next(num1)
            step += 1
        return step
    elif num1 == num2:
        return 0
    else:
        return False

print(get_next(129))
print(get_prev(134))
print(get_next_step(125,6))
print(get_prev_step(135, 6))
print(get_step(125, 135))
print(get_step(125,132))
