import sys

print(sys.argv)

for pos, arg in enumerate(sys.argv):
    print(f'Argument {pos} is {arg}')

# Use in the asc_desc function for calling classify function
# No need of multiple function calls


