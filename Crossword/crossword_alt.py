import pprint
import sys

LF, BLACK, WHITE = "\n", "#", " "

gridfile = sys.argv[1]
DEBUG = len(sys.argv) == 3

# ------------------
# Load the grid
# ------------------

grid = []
for line in open(sys.argv[1]):
    grid.append(line.strip(LF))
if DEBUG:
    pprint.pprint(grid)
    
# ------------------
# Add sentinel
# ------------------
size = len(grid)
grid.insert(0, BLACK * size)
grid.append(BLACK * size)

# ------------------
# Transpose Grid
# ------------------
transpose = []
for col in range(len(grid)):
    s = ""
    for row in grid:
        s += row[col]
    transpose.append(s)
if DEBUG:
    pprint.pprint(transpose)

# -------------------
# Combine into lines
# -------------------
across = ''.join(grid)
