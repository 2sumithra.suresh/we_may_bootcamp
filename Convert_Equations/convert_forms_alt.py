s = "7x^5 + 6x^4 + 3x^2 - 7"
def clean(eqn: str) -> str:
    return "".join(eqn.split(" "))

def terms(eqn: str) -> list[str]:
    return clean(eqn).replace("+", "$").replace("-", "$-").split("$")

def new_eqn(eqn: str) -> str:
    return "+".join(reversed(terms(eqn))).replace("+-","-")


print(terms(s))
print(s)
print(new_eqn(s))
print(new_eqn(new_eqn(s)))
