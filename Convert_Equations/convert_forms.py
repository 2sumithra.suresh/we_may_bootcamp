CARET, SPACE = "^", " "
PLUS, MINUS = "+", "-"

s = "7x^5 + 6x^4 + 3x^2 - 7"

def mathify(s: str) -> str:
    return '+'.join([make_term(t, get_var(s)) for t in new_terms(s)])

def get_var(poly: str) -> str:
    for ch in poly:
        if ch.isalpha():
            return ch

def terms(poly: str) -> list[str]:
    poly = ''.join([ch for ch in poly if ch != SPACE])
    if poly[0].isdigit():
        poly = PLUS + poly
    poly = poly.replace(MINUS, PLUS + MINUS)
    return poly.split(PLUS)

def reformat_term(term: str, var: str) -> list[int, int]:
    if var not in term:
        return 0, int(term)
    else:
        coefficient, rest = term.split(var)
        if CARET not in rest:
            return 1, int(coefficient)
        else:
            return int(rest[1:]), int(coefficient)

def new_terms(poly: str) -> list[list[int, int]]:
    return sorted([reformat_term(t,get_var(s)) for t in terms(s)[1:]])

def make_term(nt:list[int, int], var: str) -> str:
    if nt[0] == 0:
        return str(nt[1])
    elif nt[0] == 1:
        return f'{nt[1]}{var}'
    else:
        return f'{nt[1]}{var}^{nt[0]}'
        
print(get_var(s))
print(terms(s))    
print(terms(s))
print(new_terms(s))
print(s)
print(mathify(s))
print(mathify(mathify(s)))  
